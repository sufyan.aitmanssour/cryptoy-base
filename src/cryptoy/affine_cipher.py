from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    result = []
    for i in range(n):
        encrypted_char = (a * i + b) % n
        result.append(encrypted_char)
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i

    # Calculer la permutation originale en utilisant la fonction compute_permutation
    perm = compute_permutation(a, b, n)

    # Initialiser la liste result avec des valeurs par défaut (-1)
    result = [-1] * n

    # Remplir la liste result en utilisant la permutation originale
    for i, j in enumerate(perm):
        result[j] = i

    return result


def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str

    # Convertir le message en une liste d'entiers représentant les codes Unicode
    unicodes = str_to_unicodes(msg)

    # Calculer la permutation pour le chiffrement affine
    permutation = compute_permutation(
        a, b, 26
    )  # Nous utilisons 26 car nous travaillons avec l'alphabet anglais

    # Chiffrer les entiers représentant les caractères
    encrypted_unicodes = [
        permutation[code - ord("A")] + ord("A") if "A" <= chr(code) <= "Z" else code
        for code in unicodes
    ]

    # Convertir les entiers chiffrés en une chaîne de caractères chiffrée
    encrypted_msg = unicodes_to_str(encrypted_unicodes)

    return encrypted_msg


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    encrypted_msg = ""
    for char in msg:
        if char.isalpha():
            # Convertir le caractère en code Unicode (valeur numérique)
            char_code = ord(char)

            # Appliquer la formule du chiffrement affine (a * char_code + b) % 26
            if char.islower():
                encrypted_code = (a * (char_code - ord("a")) + b) % 26 + ord("a")
            else:
                encrypted_code = (a * (char_code - ord("A")) + b) % 26 + ord("A")

            # Convertir le code chiffré en caractère et l'ajouter au message chiffré
            encrypted_msg += chr(encrypted_code)
        else:
            # Si le caractère n'est pas une lettre, le laisser inchangé
            encrypted_msg += char

    return encrypted_msg


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    # Convertir le message chiffré en une liste d'entiers représentant les codes Unicode
    encrypted_unicodes = str_to_unicodes(msg)

    # Calculer la permutation inverse pour le déchiffrement affine
    inverse_permutation = compute_inverse_permutation(
        a, b, 26
    )  # Nous utilisons 26 car nous travaillons avec l'alphabet anglais

    # Déchiffrer les entiers représentant les caractères chiffrés
    decrypted_unicodes = [
        inverse_permutation[code - ord("A")] + ord("A")
        if "A" <= chr(code) <= "Z"
        else code
        for code in encrypted_unicodes
    ]

    # Convertir les entiers déchiffrés en une chaîne de caractères déchiffrée
    decrypted_msg = unicodes_to_str(decrypted_unicodes)

    return decrypted_msg


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction
    decrypted_msg = ""
    for char in msg:
        if char.isalpha():
            # Convertir le caractère en code Unicode (valeur numérique)
            char_code = ord(char)

            # Déchiffrer le caractère en utilisant la formule du déchiffrement affine
            if char.islower():
                decrypted_code = (a_inverse * (char_code - ord("a") - b)) % 26 + ord(
                    "a"
                )
            else:
                decrypted_code = (a_inverse * (char_code - ord("A") - b)) % 26 + ord(
                    "A"
                )

            # Convertir le code déchiffré en caractère et l'ajouter au message déchiffré
            decrypted_msg += chr(decrypted_code)
        else:
            # Si le caractère n'est pas une lettre, le laisser inchangé
            decrypted_msg += char

    return decrypted_msg


def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    keys = []
    for a in range(1, n):
        if gcd(a, n) == 1:
            keys.append(a)
    return keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)
    for a_1 in affine_keys:
        if (a * a_1) % n == 1:
            return a_1

    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58

    # Placer le code ici

    # Récupérer les clés valides pour le chiffrement affine avec n = 26 (pour l'alphabet anglais)
    affine_keys = compute_affine_keys(26)

    # Parcourir toutes les clés valides pour 'a'
    for a in affine_keys:
        # Calculer l'inverse de 'a' (modulaire) en utilisant la fonction 'compute_affine_key_inverse'
        a_inverse = compute_affine_key_inverse(a, affine_keys, 26)

        # Déchiffrer le message avec 'a_inverse' et 'b=58' en utilisant 'decrypt_optimized'
        decrypted_msg = decrypt_optimized(s, a_inverse, 58)

        # Vérifier si "bombe" est présent dans le message déchiffré
        if "bombe" in decrypted_msg:
            return decrypted_msg, (a_inverse, 58)

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg

    # Placer le code ici

    # Récupérer les clés valides pour le chiffrement affine avec n = 26 (pour l'alphabet anglais)
    affine_keys = compute_affine_keys(26)

    # Parcourir toutes les clés valides pour 'a'
    for a in affine_keys:
        # Calculer l'inverse de 'a' (modulaire) en utilisant la fonction 'compute_affine_key_inverse'
        a_inverse = compute_affine_key_inverse(a, affine_keys, 26)

        # Déchiffrer le message avec 'a_inverse' et 'b=58' en utilisant 'decrypt_optimized'
        decrypted_msg = decrypt_optimized(s, a_inverse, 58)

        # Vérifier si "bombe" est présent dans le message déchiffré
        if "bombe" in decrypted_msg:
            return decrypted_msg, (a_inverse, 58)

    raise RuntimeError("Failed to attack")

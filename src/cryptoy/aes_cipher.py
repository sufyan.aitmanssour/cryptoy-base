from cryptography.hazmat.primitives.ciphers.aead import (
    AESGCM,
)


def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # Créer une instance de la classe AESGCM avec la clé fournie
    aesgcm = AESGCM(key)

    # Chiffrer le message en utilisant la méthode encrypt de AESGCM
    ciphertext = aesgcm.encrypt(nonce, msg, None)

    return ciphertext


def decrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # Créer une instance de la classe AESGCM avec la clé fournie
    aesgcm = AESGCM(key)

    # Déchiffrer le message en utilisant la méthode decrypt de AESGCM
    deciphertext = aesgcm.decrypt(nonce, msg, None)

    return deciphertext

from math import (
    gcd,
)

from cryptoy.utils import (
    draw_random_prime,
    int_to_str,
    modular_inverse,
    pow_mod,
    str_to_int,
)


def keygen() -> dict:
    e = 65537
    # Tirer aléatoirement un nombre premier p
    p = draw_random_prime()

    # Tirer aléatoirement un nombre premier q
    q = draw_random_prime()

    # Calcul de n, qui est le produit de p et q
    n = p * q

    # Calcul de d, l'inverse de e modulo (p - 1) * (q - 1)
    d = modular_inverse(e, (p - 1) * (q - 1))

    # Renvoyer un dictionnaire contenant les clés publique et privée
    return {"public_key": (e, n), "private_key": d}


def encrypt(msg: str, public_key: tuple) -> int:
    # Convertir le message en nombre entier avec la fonction str_to_int
    msg_int = str_to_int(msg)

    # Vérifier que le nombre est inférieur à public_key[1] (N)
    if msg_int >= public_key[1]:
        raise ValueError("Message too long for the given public key.")

    # Chiffrer le nombre entier avec pow_mod et les paramètres de la clé publique (e, N)
    encrypted_msg = pow_mod(msg_int, public_key[0], public_key[1])

    return encrypted_msg


def decrypt(msg: int, key: dict) -> str:
    # Utiliser pow_mod avec les paramètres de la clé pour déchiffrer l'entier msg
    decrypted_msg_int = pow_mod(msg, key["private_key"], key["public_key"][1])

    # Convertir l'entier déchiffré en une chaîne de caractères avec la fonction int_to_str
    decrypted_msg = int_to_str(decrypted_msg_int)

    return decrypted_msg
